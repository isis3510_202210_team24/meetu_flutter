import 'dart:async';
import 'package:connectivity/connectivity.dart';

class ConnectionStatusService {

  static final ConnectionStatusService _singleton = ConnectionStatusService._internal();
  ConnectionStatusService._internal();
  static ConnectionStatusService get instance => _singleton;

  final Connectivity _connectivity = Connectivity();
  
  late StreamSubscription<ConnectivityResult> _subscription;

  bool _isConnected = true;

  bool get isConnected => _isConnected;

  void dispose() {
    _subscription.cancel();
  }

  void verifyInternet() async {
    ConnectivityResult connectivityResult = await (_connectivity.checkConnectivity());
    _isConnected = (connectivityResult != ConnectivityResult.none);
    _subscription = _connectivity.onConnectivityChanged.listen((ConnectivityResult connectivityResult) {
      _isConnected = !(connectivityResult == ConnectivityResult.none);
    });
  }

}