import 'dart:async';
import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:logger/logger.dart';
import 'package:meetu/app/models/profile.dart';
import 'package:meetu/app/models/question.dart';
import 'package:meetu/app/models/question_answer_item.dart';
import 'package:meetu/app/services/store_service.dart';
import 'package:meetu/app/utils/logger.dart';
import 'package:meetu/firebase_options.dart';

part 'firebase_auth.dart';
part 'firebase_firestore.dart';
part 'firebase_analitycs.dart';
class FirebaseService {

  // Singleton declaration
  static final FirebaseService _singleton = FirebaseService._internal();
  FirebaseService._internal();
  static FirebaseService get instance => _singleton;

  // Logger
  final MeetULogger logger = MeetULogger();

  // Firestore Collections
  late CollectionReference<Question> _questionsRef;

  late CollectionReference<Profile> _profileRef;

  late CollectionReference<QuestionsAnswered> _questionsAnsweredRef;

  late CollectionReference<Map<String,dynamic>> _recomendedProfilesRef;

  StreamController<List<Profile>> _recomendedProfilesController = StreamController<List<Profile>>();

  // Firebase Auth
  late FirebaseAuth _firebaseAuth;

  late String uid = "geUlk55OR9wW1e6Gw6Uh";

  

  // Constructor
  factory FirebaseService() {
    return _singleton;
  }

  // Initialize Firebase
  Future<void> initialize() async{
   await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
    initializeColectionReferences();
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
    _firebaseAuth = FirebaseAuth.instance;
  }

  

}