import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:meetu/app/utils/logger.dart';

class StoreService{

  static final StoreService _singleton = StoreService._internal();
  StoreService._internal();
  static StoreService get instance => _singleton;

  factory StoreService(){
    return instance;
  }

  final FlutterSecureStorage _secureStorage = FlutterSecureStorage();

    final Map<String, String> _keys = {
    "keyToken": "MeetUSessionToken",
    "keyUserId": "MeetUId",
  };

  
  Future<String?> getToken() async {
    final String? _token = await _secureStorage.read(key: _keys["keyToken"]!);
    return _token;
  }

  
  Future<void> setToken(String token) async {
    await _secureStorage.write(key: _keys["keyToken"]!, value: token);
  }

    @override
  Future<bool> isLogged() async {
    final String? _token = await getToken();
    MeetULogger().i('is logged: $_token');
    return (_token!=null&& _token.trim().isNotEmpty);
  }

  Future<void> logout() async {
    await _secureStorage.write(key: _keys["keyToken"]!, value: null);
    await _secureStorage.deleteAll();
  }

}