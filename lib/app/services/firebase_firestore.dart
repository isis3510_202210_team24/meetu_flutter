part of 'firebase_service.dart';
extension FirestoreService on FirebaseService{

  // initialization of general Firestore Collections references
  void initializeColectionReferences(){
    _questionsRef =  FirebaseFirestore.instance.collection('questions').withConverter<Question>(
      fromFirestore: (snapshot, _) => Question.fromJson(snapshot.data()!),
      toFirestore: (question, _) => question.toJson()
    );

    _profileRef =  FirebaseFirestore.instance.collection('profiles').withConverter<Profile>(
      fromFirestore: (snapshot, _) => Profile.fromJson(snapshot.data()!),
      toFirestore: (profile, _) => profile.toJson()
    );

    _questionsAnsweredRef =  FirebaseFirestore.instance.collection('questionsAnswered').withConverter<QuestionsAnswered>(
      fromFirestore: (snapshot, _) => QuestionsAnswered.fromJson(snapshot.data()!),
      toFirestore: (qa, _) => qa.toJson()
    );
  }

  // initialization of specific Firestore Collections references
  Future<void> initializeSpecificColectionReferences()async{
    String? uid = await StoreService().getToken();
    _recomendedProfilesRef = FirebaseFirestore.instance.collection('profiles').doc(uid).collection('recommendedProfiles');
  }

  // get questions from Firestore as a stream
  Stream<List<Question>> getQuestions(){
    return _questionsRef.snapshots().map((document) {
      return document.docs.map((e) {
       return e.data();
      }).toList();
    });
  }

  // get profile of specific user from Firestore as a stream
  Stream<Profile?> getProfile(String uid){
    return _profileRef.doc(uid).snapshots().map((document) {
      return document.data();
    });
  }

  Stream<List<Profile>> getRecomendedProfiles(){
    Stream<List<String>> profiles = _recomendedProfilesRef.snapshots().map((document) {
      return document.docs.map((e) {
       return e.data()['id'] as String;  
      }).toList();
    });

    
    StreamController<List<Profile>> controller = StreamController<List<Profile>>();
    
    profiles.listen((p) {
      List<String> p2 = p.cast<String>();
      controller.addStream(_profileRef.where(FieldPath.documentId, whereIn: p2).snapshots().map((document) {
        return document.docs.map((e) {
        return e.data();
        }).toList();
      }));

    });
    
    return controller.stream;
  }

  Future<bool> hasAnswers(){
    return _questionsAnsweredRef.doc(uid).get().then((document) {
      logger.i('hasAnswers: ${document.exists}');
      return document.exists;
    });
  }


}