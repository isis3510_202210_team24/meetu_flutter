part of 'firebase_service.dart';
extension FirebaseAuthService on FirebaseService{

  Future<String?> signUp(String email, String password) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password
      );
      return userCredential.user?.uid;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        logger.i('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        logger.i('The account already exists for that email.');
      }
    } catch (e) {
      logger.e(e);
    }
    return null;
  }

  Future<String?> signIn(String email, String password) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password
      );
      return userCredential.user?.uid;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        logger.i('No user found for that email.');
        throw Exception('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        logger.i('Wrong password provided for that user.');
        throw Exception('Wrong password provided for that user.');
      }
    }
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

}