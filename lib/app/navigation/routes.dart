import 'package:get/get.dart';
import 'package:meetu/app/ui/home/home_binding.dart';
import 'package:meetu/app/ui/home/home_view.dart';
import 'package:meetu/app/ui/main/main_binding.dart';
import 'package:meetu/app/ui/main/main_view.dart';
import 'package:meetu/app/ui/splash/splash_binding.dart';
import 'package:meetu/app/ui/splash/splash_view.dart';

class MeetUPages {
  static const String splashpage = '/';
  static const String mainpage = '/main';
  static const String homepage = '/home';
}

List<GetPage> meetuRoutes = [
  GetPage(
    name: MeetUPages.splashpage,
    page: () => const SplashPage(),
    binding: SplashBinding()
  ),
  GetPage(
    name: MeetUPages.mainpage,
    page: () => const MainPage(),
    binding: MainBinding()
  ),
  GetPage(
    name: MeetUPages.homepage,
    page: () => const HomePage(),
    binding: HomeBinding()
  ),
];