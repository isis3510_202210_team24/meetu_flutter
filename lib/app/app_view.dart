import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/app_binding.dart';
import 'package:meetu/app/navigation/routes.dart';
import 'package:meetu/app/utils/theme.dart';
import 'package:firebase_analytics/firebase_analytics.dart';


class MyApp extends StatelessWidget {

    const MyApp({Key? key}) : super(key: key);

    static FirebaseAnalytics analytics = FirebaseAnalytics.instance;
    static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);

    @override
    Widget build(BuildContext context) {
      return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        initialBinding: AppBinding(),      
        getPages: meetuRoutes,
        theme: MeetUTheme.ligththeme,
        navigatorObservers: <NavigatorObserver>[observer],
        darkTheme: MeetUTheme.darktheme,
        themeMode: ThemeMode.system,
      );
    }

}