
import 'package:logger/logger.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

class MeetULogger extends Logger{
  static final MeetULogger _singleton = new MeetULogger._internal();

  MeetULogger._internal();
  factory MeetULogger() {
    return _singleton;
  }

  @override
  void e(dynamic message, [dynamic error, StackTrace? stackTrace]) {
    FirebaseCrashlytics.instance.recordError(
      message, 
      stackTrace,
      reason: error
    );
    super.e(message, error, stackTrace);
  }
}