import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class MeetUTheme {
  
  static const Color primaryColor = Color(0xFF295A83);
  static const Color secondaryColor = Color(0xFFEF6F54);

  static ThemeData darktheme = ThemeData.dark().copyWith(
    colorScheme: const ColorScheme.dark(
      primary: primaryColor,
      secondary: secondaryColor,
    ),
    
    elevatedButtonTheme: ElevatedButtonThemeData(
      
         style: ElevatedButton.styleFrom(
          onPrimary: Colors.white,
          primary: secondaryColor,
          onSurface: Colors.white,
          elevation: 0,
          textStyle: const TextStyle(color: Colors.white),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50.0),
            
          ),
        ),
        
    ),
    
    
  );

    static ThemeData ligththeme = ThemeData.light().copyWith(
    colorScheme: const ColorScheme.light(
      primary: primaryColor,
      secondary: secondaryColor,
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      
         style: ElevatedButton.styleFrom(
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50.0),
            
          ),
        ),
        
    ),
    
    
  );

}