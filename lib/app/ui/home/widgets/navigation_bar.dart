import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/ui/home/home_controller.dart';
import 'package:meetu/app/ui/main/main_controller.dart';
import 'package:meetu/app/ui/main/widgets/register_form.dart';
import 'package:meetu/app/utils/theme.dart';

class NavigationBarHome extends GetWidget<HomeController>{
  const NavigationBarHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(                                             
      decoration:const  BoxDecoration(                                                   
        borderRadius: BorderRadius.only(                                           
          topRight: Radius.circular(30), topLeft: Radius.circular(30)),            
        boxShadow: [                                                               
          BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),       
        ],                                                                         
      ),                                                                           
      child: ClipRRect(                                                            
        borderRadius:const  BorderRadius.only(                                           
        topLeft: Radius.circular(30.0),                                            
        topRight: Radius.circular(30.0),                                           
        ),                                                                         
        child: Obx(
          ()=> BottomNavigationBar(     
            currentIndex: controller.tabIndex.value,  
             
            onTap: controller.changeTabIndex,                                        
            items:const <BottomNavigationBarItem>[                                        
              BottomNavigationBarItem(                                               
                icon: Icon(Icons.person), label: 'Profile'),               
              BottomNavigationBarItem(                                               
                icon: Icon(Icons.auto_awesome_motion_rounded), label: 'Explore'), 
              BottomNavigationBarItem(                                               
                icon: Icon(Icons.calendar_today), label: 'Dates')                 
            ],                                                                       
            ),
        ),                                                                         
  )                                                                            
);
  }
  
}