
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/ui/main/main_controller.dart';

class QuestionsPop extends GetWidget<MainController>{

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.1,
        vertical: MediaQuery.of(context).size.height * 0.1,
        
      ),
      child: Card(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("Do you have questions?"),
            const SizedBox(height: 20),
            Text("We are here to help!"),
            const SizedBox(height: 20),
            Text("Call us at +91-9888888888"),
            const SizedBox(height: 20),
            Text("Email us at")
          ],
        ),
      ),
      );
  }

}