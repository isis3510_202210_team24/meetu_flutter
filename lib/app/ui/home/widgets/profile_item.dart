
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/models/profile.dart';
import 'package:meetu/app/ui/main/main_controller.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ProfileItem extends GetWidget<MainController>{
  final Profile profile;
  const ProfileItem({Key? key,required  this.profile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = Get.width;
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
      ),
      child: Stack(
      children: [
          Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Center(
            child: CachedNetworkImage(
            imageUrl: profile.photoUrl,
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                    ),
              ),
            ),
            progressIndicatorBuilder: (context, url, downloadProgress) => 
                CircularProgressIndicator(value: downloadProgress.progress),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
          ),
      ),
      Positioned(
        child: SizedBox.square(dimension: width/9,child: Container(
        decoration: BoxDecoration(
          color: ([...Colors.primaries]..shuffle()).first,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(20)),
        ),
      ),), bottom:0, left: 0,),
      Align(
        child: SizedBox.square(dimension: width/9,child: Container(
        color: ([...Colors.accents]..shuffle()).first,
        ),), 
        alignment: Alignment.bottomCenter,
      ),
      Positioned(child: SizedBox.square(dimension: width/9,child: Container(
        decoration: BoxDecoration(
          color: ([...Colors.primaries]..shuffle()).first,
          borderRadius: const BorderRadius.only(topRight: Radius.circular(20)),
        ),
      ),), bottom:0, right: 0,),
      ],
      
    )
    );
  }
  
}