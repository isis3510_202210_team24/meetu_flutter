import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:meetu/app/models/profile.dart';
import 'package:meetu/app/ui/home/home_controller.dart';

class ProfileView extends GetView<HomeController> {

  final Stream<Profile?> profile;
  final bool self;

  const ProfileView({required this.profile, required this.self});

  @override
  Widget build(BuildContext context) {
    
    return const Center(child: Text('Profile'));
  }
}