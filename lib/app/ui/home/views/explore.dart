import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:meetu/app/models/profile.dart';
import 'package:meetu/app/services/firebase_service.dart';
import 'package:meetu/app/ui/home/home_controller.dart';
import 'package:meetu/app/ui/home/widgets/profile_item.dart';

class ExploreView extends GetView<HomeController> {

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Profile>>(
        stream: FirebaseService.instance.getRecomendedProfiles(),
        builder: (BuildContext context, AsyncSnapshot<List<Profile>> snapshot){
          if(!snapshot.hasData){
            return const Center(child: CircularProgressIndicator());
          }

          return GridView.count(
            crossAxisCount: 3,
            childAspectRatio: 3/4,
            children: snapshot.data!.map(
              (profile) =>  ProfileItem(profile: profile)
              ).toList()
            );
        }
      );
  }
}