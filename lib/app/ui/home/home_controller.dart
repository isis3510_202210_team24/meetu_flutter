import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/navigation/routes.dart';
import 'package:meetu/app/services/firebase_service.dart';
import 'package:meetu/app/ui/home/widgets/questions.dart';


class HomeController extends GetxController {

  var tabIndex = 1.obs; 

  void changeTabIndex(int index) {
    tabIndex.value = index;
  }

  void checkQuestions(){
    FirebaseService.instance.hasAnswers().then((value) {
      if(!value){
        showDialog(
          context: Get.context!,
          builder: (BuildContext context) {
            return QuestionsPop();
          },
        );
      }
    });
  }


}