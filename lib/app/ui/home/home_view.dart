import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/models/profile.dart';
import 'package:meetu/app/navigation/routes.dart';
import 'package:meetu/app/services/firebase_service.dart';
import 'package:meetu/app/ui/home/home_controller.dart';
import 'package:meetu/app/ui/home/views/dates.dart';
import 'package:meetu/app/ui/home/views/explore.dart';
import 'package:meetu/app/ui/home/views/profile.dart';
import 'package:meetu/app/ui/home/widgets/navigation_bar.dart';
import 'package:meetu/app/ui/home/widgets/profile_item.dart';

class HomePage extends GetView<HomeController>{
  @override
  const HomePage({Key? key}) : super(key: key);

  

  @override
  Widget build(BuildContext context) {
    
    //controller.checkQuestions();
    return Scaffold(
      extendBody: true,
      bottomNavigationBar: const NavigationBarHome(),
      body:Obx((() => IndexedStack(
        index: controller.tabIndex.value,
        children: <Widget>[
          ProfileView(profile: FirebaseService.instance.getProfile(FirebaseService.instance.uid), self: true,),
          ExploreView(),
          DatesView(),
        ]
      ))
      ));
      
  }

}