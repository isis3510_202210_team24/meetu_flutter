import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/ui/main/main_controller.dart';
import 'package:meetu/app/ui/main/widgets/login_form.dart';
import 'package:meetu/app/ui/main/widgets/mail_confirm.dart';
import 'package:meetu/app/ui/main/widgets/password_recovery.dart';
import 'package:meetu/app/ui/main/widgets/register_form.dart';
import 'package:meetu/app/utils/theme.dart';

class FormContainer extends GetWidget<MainController>{
  const FormContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      ()=>
      controller.selectedIndex==0?
      const LoginForm():
      controller.selectedIndex==1?
      const RegisterForm():
      controller.selectedIndex==2?
      const MailConfirm():
      const PasswordRecovery()
      );
  }
  
}