import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker_bdaya/flutter_datetime_picker_bdaya.dart';
import 'package:get/get.dart';
import 'package:meetu/app/ui/main/main_controller.dart';
import 'package:meetu/app/utils/theme.dart';

class RegisterForm extends GetWidget<MainController>{
  const RegisterForm({Key? key}) : super(key: key);

 @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 80),
        child: SingleChildScrollView(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset("assets/logo.png",width: MediaQuery.of(context).size.width*0.5,),
          TextField(
           decoration: const InputDecoration(
              labelText: 'Email',
            ), 
            controller: controller.mailController,
          ),
          TextField(
            obscureText: true,
            decoration: const InputDecoration(
              labelText: 'Password',
            ),
            controller: controller.passwordController,
          ),
          TextField(
            obscureText: true,
            decoration: const InputDecoration(
              labelText: 'Confirm Password',
            ),
            controller:  controller.confirmPasswordController,
          ),
          TextButton(
              onPressed: () {
                DatePicker.showDatePicker(context,
                                      showTitleActions: true,
                                      minTime: DateTime(1980, 1, 1),
                                      maxTime: DateTime.now().add(const Duration(days: -6580)), onChanged: (date) {
                                  }, onConfirm: (date) {
                                  }, currentTime: DateTime.now());
            },
            child: const Text(
                'Input Birth Date',
                style: TextStyle(color: Colors.blue),
            )),
          const SizedBox(height: 20),
          SizedBox(
            width: 800,
            child: ElevatedButton(
              onPressed: ()=>controller.register(), 
              child: const Text("Register"),
            ),
          ),
          const SizedBox(height: 10),
          GestureDetector(
            onTap: ()=>controller.openLogin(),
            child: const Text.rich(TextSpan(
            text: "Already have an account?",
            style: TextStyle(
              color: Colors.blue,
              fontSize: 14,
            ),
          )),
          ),
          const SizedBox(height: 10),
          IconButton(onPressed: ()=>controller.toggle(), icon: const Icon(Icons.arrow_downward))
        ],
      ),
        )
      )
    );
  }
  
}