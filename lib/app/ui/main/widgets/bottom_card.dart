import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/ui/main/main_controller.dart';
import 'package:meetu/app/ui/main/widgets/form_container.dart';
import 'package:meetu/app/utils/theme.dart';

class BottomCard extends GetWidget<MainController>{
  const BottomCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(()=>AnimatedContainer(
      duration: const Duration(milliseconds: 200),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
        color: Get.isDarkMode?Colors.black:Theme.of(context).backgroundColor
         
      ),
      height: controller.isOpen? MediaQuery.of(context).size.height * 0.9 : 100,
      width: MediaQuery.of(context).size.width,
      child: controller.isOpen?
      const FormContainer():
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            width: 120,
            child: ElevatedButton(
              onPressed: ()=>controller.openLogin(), 
              child: Text("Log In"),
            ),
          ),
          SizedBox(
            width: 120,
            child: ElevatedButton(
              onPressed: ()=>controller.openRegister(), 
              child: Text("Register"),
            ),
          ),

        ],
      )
        ],
      ),
    ));
  }
  
}