import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/ui/main/main_controller.dart';

class MailConfirm extends GetWidget<MainController>{
  const MailConfirm({Key? key}) : super(key: key);

 @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 80),
        child: SingleChildScrollView(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text("¡You have mail!", 
                      style: 
            TextStyle(
              fontWeight: FontWeight.bold, 
              fontSize: 20
            ),
            textAlign: TextAlign.center,
          ),
          Image.asset("assets/logo.png",width: MediaQuery.of(context).size.width*0.3,),
          const Text("We sent you an email to the mail:", 
            style: 
            TextStyle(
              fontWeight: FontWeight.bold, 
              fontSize: 20
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 20,),
          SizedBox(
            width: 200,
            child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.white, width: 2),
              borderRadius: BorderRadius.circular(30)
            ),
            child:  Text(controller.mailController.text, 
          
            style: 
            const TextStyle(
              fontWeight: FontWeight.bold, 
              fontSize: 20
            ),
            textAlign: TextAlign.center,
          ),
          ),
          ),
          const SizedBox(height: 20,),
          const Text("Check your mail and confirm your account", 
            style: 
            TextStyle(
              fontWeight: FontWeight.bold, 
              fontSize: 20
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 10),
          IconButton(onPressed: ()=>controller.toggle(), icon: const Icon(Icons.arrow_downward)),
          
          const SizedBox(height: 10),
          GestureDetector(
            onTap: ()=>controller.openLogin(),
            child: const Text.rich(
            TextSpan(
            text: "Didnt receive a mail?",        
            style: TextStyle(
              color: Colors.blue,
              fontSize: 14,
            ),
            children: [
              TextSpan(
                text: " ask for a new one",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.bold
                )
              )
            ],
            
          ),
          textAlign: TextAlign.center,
          ),
          ),

        ],
      ),
        )
      )
    );
  }
  
}