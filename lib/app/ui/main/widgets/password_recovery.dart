import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/ui/main/main_controller.dart';
import 'package:meetu/app/utils/theme.dart';

class PasswordRecovery extends GetWidget<MainController>{
  const PasswordRecovery({Key? key}) : super(key: key);

 @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 80),
        child: SingleChildScrollView(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text("Forgot your password?", 
                      style: 
            TextStyle(
              fontWeight: FontWeight.bold, 
              fontSize: 20
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 10),
          Image.asset("assets/logo.png",width: MediaQuery.of(context).size.width*0.3,),
          const SizedBox(height: 10),
          const Text("Give us your email and well send a recovery link to your email", 
                      style: 
            TextStyle(
              fontWeight: FontWeight.bold, 
              fontSize: 20
            ),
            textAlign: TextAlign.center,
          ),
                    const TextField(
           decoration: InputDecoration(
              labelText: 'Email',
            ), 
          ),
          const SizedBox(height: 20),
          SizedBox(
            width: 800,
            child: ElevatedButton(
              onPressed: ()=>controller.openMailConfirm(), 
              child: const Text("Recover Password"),
            ),
          ),
          const SizedBox(height: 20),
          IconButton(onPressed: ()=>controller.toggle(), icon: const Icon(Icons.arrow_back_ios_new))
        ],
      ),
        )
      )
    );
  }
  
}