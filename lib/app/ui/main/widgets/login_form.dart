import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/ui/main/main_controller.dart';
import 'package:meetu/app/ui/main/widgets/register_form.dart';
import 'package:meetu/app/utils/theme.dart';

class LoginForm extends GetWidget<MainController>{
  const LoginForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 80),
        child: SingleChildScrollView(
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset("assets/logo.png",width: MediaQuery.of(context).size.width*0.5,),
             TextField(
             decoration: const InputDecoration(
                labelText: 'Email',
              ), 
              controller: controller.mailController,
            ),
            TextField(
              obscureText: true,
              decoration: const InputDecoration(
                labelText: 'Password',
              ),
              controller: controller.passwordController,
            ),
            const SizedBox(height: 20),
            GestureDetector(
              onTap: ()=>controller.openPasswordRecovery(),
              child: const Text.rich(TextSpan(
              text: "Forgot your password?",
              style: TextStyle(
                color: Colors.blue,
                fontSize: 14,
              ),
            )),
            ),
            const SizedBox(height: 80),
            SizedBox(
              width: 800,
              child: ElevatedButton(
                onPressed: ()=>controller.login(), 
                child: const Text("Log In"),
              ),
            ),
            const SizedBox(height: 10),
            SizedBox(
              width: 800,
              child: ElevatedButton(
                onPressed: ()=>controller.login(), 
                child: const Text("Log In with Microsoft"),
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  onPrimary: MeetUTheme.primaryColor,
                  
                )
              ),
            ),
            const SizedBox(height: 10),
            GestureDetector(
              onTap: ()=>controller.openRegister(),
              child: const Text.rich(TextSpan(
              text: "Dont have an account?",
              style: TextStyle(
                color: Colors.blue,
                fontSize: 14,
              ),
            )),
            ),
            IconButton(onPressed: ()=>controller.toggle(), icon: const Icon(Icons.arrow_downward))
          ],
              ),
        ),
      )
    );
  }
  
}