import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/navigation/routes.dart';
import 'package:meetu/app/services/firebase_service.dart';
import 'package:meetu/app/services/store_service.dart';

class MainController extends GetxController {

  final RxBool _isOpen = RxBool(false);
  bool get isOpen => _isOpen.value;
  set isOpen(bool value) => _isOpen.value = value;

  final RxInt _selectedIndex = RxInt(0);
  int get selectedIndex => _selectedIndex.value;
  set selectedIndex(int value) => _selectedIndex.value = value;

  final RxString _mail = RxString("mail@mail.com");
  String get mail => _mail.value;
  set mail(String value) => _mail.value = value;
  

  TextEditingController mailController = TextEditingController();
  

  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();


  @override
  void onReady() {
    super.onReady();
  }

  void toggle() {
    isOpen = !isOpen;
  }

  void openLogin() {
    isOpen = true;
    selectedIndex = 0;
  }

  void openMailConfirm() {
    isOpen = true;
    selectedIndex = 2;
  }

  void openPasswordRecovery(){
    isOpen = true;
    selectedIndex = 3;
  }

  void login() async {
    try {
      String? uid = await FirebaseService.instance.signIn(mailController.text, passwordController.text);
      if (uid != null) {
        await FirebaseService.instance.initializeSpecificColectionReferences();
        StoreService().setToken(uid);
        Get.offAllNamed(MeetUPages.homepage);
      }
    } on Exception catch ( e) {
      SnackBar snackBar = SnackBar(
        content: Text(e.toString()),
      );
      ScaffoldMessenger.of(Get.context!).showSnackBar(snackBar);
    }
    
  }

  void register() async {
    try {
      if(passwordController.text!=confirmPasswordController.text){
        throw Exception("Passwords do not match");
      }
      String? uid = await FirebaseService.instance.signUp(mailController.text, passwordController.text);
      if (uid != null) {
        openMailConfirm();
      }
    } on Exception catch ( e) {
      SnackBar snackBar = SnackBar(
        content: Text(e.toString()),
      );
      ScaffoldMessenger.of(Get.context!).showSnackBar(snackBar);
    }
  }

  void openRegister() {
    isOpen = true;
    selectedIndex = 1;
  }
}