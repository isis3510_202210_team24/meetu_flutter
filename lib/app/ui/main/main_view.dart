
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/ui/main/main_controller.dart';
import 'package:meetu/app/ui/main/widgets/bottom_card.dart';

class MainPage extends GetView<MainController> {

  @override
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      /*
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          controller.toggle();
        },
        child: Obx(()=>Icon(controller.isOpen? Icons.toggle_on_outlined: Icons.toggle_off_outlined),)
      ),
      */
      body: Stack(
        children: [
          SizedBox.expand(
            child: ImageFiltered(
              imageFilter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
              child: Image.asset('assets/openbg.jpg', fit: BoxFit.fitHeight,),
            ),
          ),
          Obx(
            ()=>
            controller.isOpen ? 
            SizedBox.shrink():Positioned(
            top: 50,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: Image.asset("assets/logo.png",width: MediaQuery.of(context).size.width*0.5,),
              )
            )
          )
    
          ) ,
          
           const Align(
            alignment: Alignment.bottomRight,
            child: BottomCard(),
          )
        ],
      ),
    );
  }

}