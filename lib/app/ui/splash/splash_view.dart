import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meetu/app/ui/splash/splash_controller.dart';
import 'package:meetu/app/utils/theme.dart';
import 'package:rive/rive.dart';

class SplashPage extends GetView<SplashController>{
  @override
  const SplashPage({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    controller.loadSplash();
    return const Scaffold(
      backgroundColor: MeetUTheme.primaryColor,
      body: Center(
        child: RiveAnimation.asset('assets/splash.riv', antialiasing: true,)
      ),
    );
  }

}