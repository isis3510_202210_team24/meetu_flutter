import 'package:get/get.dart';
import 'package:meetu/app/navigation/routes.dart';
import 'package:meetu/app/services/firebase_service.dart';
import 'package:meetu/app/services/store_service.dart';

class SplashController extends GetxController {

  void loadSplash()async{
    Future.delayed(const Duration(seconds: 3), () {
      StoreService().isLogged().then((value) async{
      if(value){
        await FirebaseService.instance.initializeSpecificColectionReferences();
        Get.offAllNamed(MeetUPages.homepage);
      }else{
         Get.offAllNamed(MeetUPages.mainpage);
      }
    });
    });
  }


}