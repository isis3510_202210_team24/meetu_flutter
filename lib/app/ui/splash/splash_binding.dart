import 'package:get/get.dart';
import 'package:meetu/app/ui/splash/splash_controller.dart';

class SplashBinding implements Bindings {

  @override
  void dependencies() {
    Get.put(SplashController());
  }
}