class Profile{
  final int age;
  final String major;
  final String name;
  final String photoUrl;

  Profile({
    required this.age,
    required this.major,
    required this.name,
    required this.photoUrl
  });

  Profile.fromJson(Map<String, dynamic> json)
    : age = json['age'],
      major = json['major'],
      name = json['name'],
      photoUrl = json['photoUrl'];

  Map<String, dynamic> toJson() => {
    'age': age,
    'major': major,
    'name': name,
    'photoUrl': photoUrl
  };

  @override
  String toString() {
    return '{age: $age, major: $major, name: $name, photoUrl: $photoUrl}';
  }
}