class QuestionAnswerItem {
  String answer;
  String question;

  QuestionAnswerItem({
    required this.answer,
    required this.question
  });

  QuestionAnswerItem.fromJson(Map<String, dynamic> json)
    : answer = json['answer'],
      question = json['question'];

  Map<String, dynamic> toJson() => {
    'answer': answer,
    'question': question
  };
}

class QuestionsAnswered{

  List<QuestionAnswerItem> questionsAnswered;

  QuestionsAnswered({
    required this.questionsAnswered
  });

  QuestionsAnswered.fromJson(Map<String, dynamic> json)
    : questionsAnswered = (json['questionsAnswered'] as List<dynamic>).map((e) => QuestionAnswerItem.fromJson(e)).toList();

  Map<String, dynamic> toJson() => {
    'questionsAnswered': questionsAnswered.map((e) => e.toJson()).toList()
  };
}