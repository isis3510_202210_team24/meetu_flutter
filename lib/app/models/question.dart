class Question{
  final String question;
  final List<String> options;

  Question({
    required this.question,
    required this.options
  });
  
  Question.fromJson(Map<String, dynamic> json)
    : question = json['question'],
      options = json['options'].cast<String>();
  
  Map<String, dynamic> toJson() => {
    'question': question,
    'options': options
  };

  @override
  String toString() {
    return '{question: $question, options: $options}';
  }
}