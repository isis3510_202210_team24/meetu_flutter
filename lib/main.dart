import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:meetu/app/app_view.dart';
import 'package:meetu/app/services/firebase_service.dart';

Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();

  await FirebaseService().initialize();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp
  ]).then((value) => runApp(const MyApp()));

}


